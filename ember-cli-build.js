/*jshint node:true*/
/* global require, module */
var EmberApp = require('ember-cli/lib/broccoli/ember-app');

module.exports = function(defaults) {
  var app = new EmberApp(defaults, {
    bootstrap: { //Let's not use the bootstrap js
      plugins: []
    }
    // Add options here
  });

  app.import('bower_components/tether/dist/js/tether.js');

  return app.toTree();
};
