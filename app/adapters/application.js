import RESTAdapter from 'ember-data/adapters/rest';

export default RESTAdapter.extend({
  host: 'https://api.myjson.com/bins/',
  pathForType(type) {
    if(type === 'program') {
      return '5bdb3';
    }  else if (type === 'pricing-option') {
      return '17oy7';
    } 
  }
});