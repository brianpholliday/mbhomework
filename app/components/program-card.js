import Ember from 'ember';

export default Ember.Component.extend({
  isExpanded: false,
  classNames: 'program-card',
  actions: {
    toggleExpand() {
      this.toggleProperty('isExpanded');
    }
  }
});
