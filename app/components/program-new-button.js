import Ember from 'ember';

export default Ember.Component.extend({
  isDisplayProgramNewModal: null,
  click() {
    this.set('isDisplayProgramNewModal', true);
  }
});
