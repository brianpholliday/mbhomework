import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { belongsTo, hasMany } from 'ember-data/relationships';

export default Model.extend({
  pricingOptions:    hasMany('pricingOption'),
  ProgramID:         attr(),
  Name:              attr(),
  TotalMonthlySales: attr(),
  MonthlyAttendance: attr(),
  Sales:             attr() 
});

