import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return Ember.RSVP.hash({
      programs:       this.store.findAll('program'),
      pricingOptions: this.store.findAll('pricingOption')
    });
  },
  setupController(controller, model) {
    var programs = this.store.peekAll('program');
    this.controller.set('featuredPrograms', programs.toArray().splice(0, 3));
    this.controller.set('programs', programs.toArray().splice(3, 4));  
  }
});