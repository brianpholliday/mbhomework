import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
}); 

Router.map(function() {
  this.modal('program-new-modal', {             
    withParams: ['isDisplayProgramNewModal'],
    dialogClass: 'program-new-modal'          
  });
});

export default Router;
