import DS from 'ember-data';

export default DS.RESTSerializer.extend({
  primaryKey: 'ProgramID',
  normalizeArrayResponse(store, primaryModelClass, payload, id, requestType) {
        //There's no root json in these json bins, so let's add them!
        var pluralTypeKey = Ember.Inflector.inflector.pluralize(primaryModelClass.modelName);
        var data = [];
        data[pluralTypeKey] = payload;

        return this._normalizeResponse(store, primaryModelClass, data, id, requestType, false);
    }
});