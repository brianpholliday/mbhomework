export default Ember.Helper.extend({
  compute(params, ) {
    var value           = params[0];
    var isTrailingZeros = params[1]
    var currency;
    
    currency = '$' + Number(value.toFixed(2)).toLocaleString();
    
    if(currency.indexOf(".") == -1 && isTrailingZeros) {
      currency += '.00';
    }
    
    return currency;
  }
});