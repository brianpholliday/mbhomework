# FE Homework
             
       __  ___   ____   _  __   ___    ___   ____    ___  __  __
      /  |/  /  /  _/  / |/ /  / _ \  / _ ) / __ \  / _ \ \ \/ /
     / /|_/ /  _/ /   /    /  / // / / _  |/ /_/ / / // /  \  / 
    /_/  /_/  /___/  /_/|_/  /____/ /____/ \____/ /____/   /_/  
                                                            

## Useful Links

* [ember.js](http://emberjs.com/)
* [ember-cli](http://ember-cli.com/)
* Development Browser Extensions
  * [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
  * [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)

